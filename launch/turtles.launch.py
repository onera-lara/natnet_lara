import launch
import launch.actions
import launch.substitutions
import launch_ros.actions


def generate_launch_description():

    params = {"server": "134.212.235.137",
              "local": "134.212.244.210",
              "geoproj": False,
              "log_level": "debug"
              }

    return launch.LaunchDescription([
        launch_ros.actions.SetRemap(src='/tf', dst='tf'),
        launch_ros.actions.SetRemap(src='/tf_static', dst='tf_static'),
        launch_ros.actions.Node(
            package='natnet_lara', executable='natnet_client', output='screen',
            name='natnet_client',
            namespace="Turtle_1",
            parameters=[params, {"body_name": "Turtle_1"}]),
        launch_ros.actions.Node(
            package='natnet_lara', executable='natnet_client', output='screen',
            name='natnet_client',
            namespace="Turtle_2",
            parameters=[params, {"body_name": "Turtle_2"}]),
        launch_ros.actions.Node(
            package='natnet_lara', executable='natnet_client', output='screen',
            name='natnet_client',
            namespace="Turtle_3",
            parameters=[params, {"body_name": "Turtle_3"}]),
        launch_ros.actions.Node(
            package='natnet_lara', executable='natnet_client', output='screen',
            name='natnet_client',
            namespace="Turtle_4",
            parameters=[params, {"body_name": "Turtle_4"}]),
        launch_ros.actions.Node(
            package='natnet_lara', executable='natnet_client', output='screen',
            name='natnet_client',
            namespace="Turtle_5",
            parameters=[params, {"body_name": "Turtle_5"}]),
        launch_ros.actions.Node(
            package='natnet_lara', executable='natnet_client', output='screen',
            name='natnet_client',
            namespace="Turtle_6",
            parameters=[params, {"body_name": "Turtle_6"}]),
    ])
