import launch
import launch.actions
import launch.substitutions
import launch_ros.actions


def generate_launch_description():
    return launch.LaunchDescription([
        launch_ros.actions.Node(
            package='natnet_lara', executable='natnet_client', output='screen',
            name='natnet_client',
            parameters=[{
                "server": "134.212.235.137",
                "local": "134.212.244.210",
                "reference_latitude": 44.2743,
                "reference_longitude": 1.7285,
                "reference_altitude": 361.0,
            }]),
    ])
