#include <cstdio>
#include <NatNetTypes.h>
#include <NatNetCAPI.h>
#include <NatNetClient.h>
#include "client_node.hpp"

// ROS2 node
std::shared_ptr<NatNetNode> node;
// MessageHandler receives NatNet error/debug messages
void NATNET_CALLCONV MessageHandler( Verbosity msgType, const char* msg )
{
  node->message_handler(msgType, msg);
}

void NATNET_CALLCONV DataHandler(sFrameOfMocapData* data, __attribute__((unused)) void* pUserData)
{
  node->data_handler(data);
}

int main(int argc, char ** argv)
{
  rclcpp::init(argc, argv);
  // ROS2 Node
  node = std::make_shared<NatNetNode>("natnet_lara_client");
  // Install logging callback
  NatNet_SetLogCallback(MessageHandler);
  // Connect to Motive
  node->init_params();
  if (! node->connect()) {
    return 1;
  }
  // Init data
  if (! node->get_data()) {
    return 1;
  }
  // this function will receive data from the server
  node->g_pClient.SetFrameReceivedCallback(DataHandler, (void*) &(node->g_pClient));
  
  // SPIN
  rclcpp::spin(node);
  rclcpp::shutdown();
  return 0;
}
