#include <rclcpp/rclcpp.hpp>
#include <geometry_msgs/msg/pose_stamped.hpp>
#include <geographic_msgs/msg/geo_point.hpp>
#include <geometry_msgs/msg/transform_stamped.hpp>
#include <tf2_ros/transform_broadcaster.h>

#include <cs_tools/geo_proj.hpp>

#include <NatNetTypes.h>
#include <NatNetCAPI.h>
#include <NatNetClient.h>


class NatNetNode : public rclcpp::Node {

public:
    NatNetClient g_pClient;

    NatNetNode(std::string name) : rclcpp::Node(name) {
        this->declare_parameter("server", "127.0.0.1");
        this->declare_parameter("local", "127.0.0.1");
        this->declare_parameter("geoproj", false);
        this->declare_parameter("reference_latitude", 44.2743);
        this->declare_parameter("reference_longitude", 1.7285);
        this->declare_parameter("reference_altitude", 361.0);
        this->declare_parameter("world_frame", "map");
        this->declare_parameter("robot_frame", "base_footprint");
        this->declare_parameter("body_name", "Turtle_2");

        // print version info
        unsigned char ver[4];
        NatNet_GetVersion( ver );
        RCLCPP_INFO(this->get_logger(), "NatNet LaRA Client (NatNet ver. %d.%d.%d.%d)", ver[0], ver[1], ver[2], ver[3]);

        // publishers
        pose_pub = this->create_publisher<geometry_msgs::msg::PoseStamped>("pose", qos);
        tf_br = std::make_shared<tf2_ros::TransformBroadcaster>(this);
        // Initialize Proj
        if (this->get_parameter("geoproj").as_bool()) {
            RCLCPP_DEBUG(this->get_logger(), "Initializing GeoProj origin");
            cs_tools::GeoPoint origin;
            origin.latitude = this->get_parameter("reference_latitude").as_double();
            origin.longitude = this->get_parameter("reference_longitude").as_double();
            origin.altitude = this->get_parameter("reference_altitude").as_double();
            RCLCPP_INFO(this->get_logger(), "Projection origin: %s", origin.toString().c_str());
            proj = std::make_shared<cs_tools::GeoProj>(cs_tools::WGS84, origin);
            geopoint_pub = this->create_publisher<geographic_msgs::msg::GeoPoint>("geopoint", qos);
        }
        // set the frame callback handler
        world_frame = this->get_parameter("world_frame").as_string();
        robot_frame = this->get_parameter("robot_frame").as_string();
        RCLCPP_INFO(this->get_logger(), "TF frames: %s %s", world_frame.c_str(), robot_frame.c_str());
    }

    // connection params
    void init_params() {
        g_connectParams.connectionType = ConnectionType_Multicast;
        std::string server_address = this->get_parameter("server").as_string();
        std::string local_address = this->get_parameter("local").as_string();
        g_connectParams.serverAddress = server_address.c_str();
        g_connectParams.localAddress = local_address.c_str();
        RCLCPP_INFO(this->get_logger(), "Server address: %s", g_connectParams.serverAddress);
        RCLCPP_INFO(this->get_logger(), "Local address: %s", g_connectParams.localAddress);
    }

    void message_handler( Verbosity msgType, const char* msg ) {
        switch ( msgType ) {
            case Verbosity_Debug:
                RCLCPP_DEBUG(this->get_logger(), "[NatNetLib] %s", msg);
                break;
            case Verbosity_Info:
                RCLCPP_INFO(this->get_logger(), "[NatNetLib] %s", msg);
                break;
            case Verbosity_Warning:
                RCLCPP_WARN(this->get_logger(), "[NatNetLib] %s", msg);
                break;
            case Verbosity_Error:
                RCLCPP_ERROR(this->get_logger(), "[NatNetLib] %s", msg);
                break;
            default:
                break;
        }
    }

    // Establish a NatNet Client connection
    bool connect() {
        // Release previous server
        g_pClient.Disconnect();
        // Init Client and connect to NatNet server
        int retCode = g_pClient.Connect( g_connectParams );
        if (retCode != ErrorCode_OK) {
            RCLCPP_ERROR(this->get_logger(), "Unable to connect to server.  Error code: %d. Exiting", retCode);
            return false;
        } else {
            // connection succeeded
            void* pResult;
            int nBytes = 0;
            ErrorCode ret = ErrorCode_OK;
            // print server info
            memset( &g_serverDescription, 0, sizeof( g_serverDescription ) );
            ret = g_pClient.GetServerDescription( &g_serverDescription );
            if ( ret != ErrorCode_OK || ! g_serverDescription.HostPresent )
            {
                RCLCPP_ERROR(this->get_logger(), "Unable to connect to server. Host not present. Exiting.");
                return false;
            }
            RCLCPP_INFO(this->get_logger(), "Application: %s (ver. %d.%d.%d.%d)", 
                g_serverDescription.szHostApp, g_serverDescription.HostAppVersion[0],
                g_serverDescription.HostAppVersion[1], g_serverDescription.HostAppVersion[2],
                g_serverDescription.HostAppVersion[3]);
            RCLCPP_INFO(this->get_logger(), "NatNet Version: %d.%d.%d.%d", 
                g_serverDescription.NatNetVersion[0], g_serverDescription.NatNetVersion[1],
                g_serverDescription.NatNetVersion[2], g_serverDescription.NatNetVersion[3]);
            RCLCPP_INFO_STREAM(this->get_logger(), "Client IP: " << g_connectParams.localAddress );
            RCLCPP_INFO_STREAM(this->get_logger(), "Server IP: " << g_connectParams.serverAddress );
            RCLCPP_INFO_STREAM(this->get_logger(), "Server Name: " << g_serverDescription.szHostComputerName);
            // get mocap frame rate
            ret = g_pClient.SendMessageAndWait("FrameRate", &pResult, &nBytes);
            if (ret == ErrorCode_OK)
            {
                float fRate = *((float*)pResult);
                RCLCPP_INFO(this->get_logger(), "Mocap Framerate : %3.2f", fRate);
            }
            else
                RCLCPP_WARN(this->get_logger(), "Error getting frame rate.");
            // get # of analog samples per mocap frame of data
            ret = g_pClient.SendMessageAndWait("AnalogSamplesPerMocapFrame", &pResult, &nBytes);
            if (ret == ErrorCode_OK)
            {
                g_analogSamplesPerMocapFrame = *((int*)pResult);
                RCLCPP_INFO(this->get_logger(), "Analog Samples Per Mocap Frame : %d", g_analogSamplesPerMocapFrame);
            }
            else
                RCLCPP_WARN(this->get_logger(), "Error getting Analog frame rate.");
        }
        RCLCPP_INFO(this->get_logger(), "Client initialized and ready.");
        return true;
    }

    // Retrieve Data Descriptions from Motive
    bool get_data() {
        std::string body = this->get_parameter("body_name").as_string();
    	RCLCPP_INFO(this->get_logger(), "Requesting Data Descriptions...");
	    sDataDescriptions* pDataDefs = NULL;
	    int iResult = g_pClient.GetDataDescriptionList(&pDataDefs);
	    if (iResult != ErrorCode_OK || pDataDefs == NULL)
	    {
		    RCLCPP_ERROR(this->get_logger(), "Unable to retrieve Data Descriptions.");
            return false;
	    }
        RCLCPP_INFO(this->get_logger(), "Received %d Data Descriptions:", pDataDefs->nDataDescriptions );
        for(int i=0; i < pDataDefs->nDataDescriptions; i++)
        {
            RCLCPP_DEBUG(this->get_logger(), "Data Description # %d (type=%d)", i, pDataDefs->arrDataDescriptions[i].type);
            if(pDataDefs->arrDataDescriptions[i].type == Descriptor_RigidBody)
            {
                // RigidBody
                sRigidBodyDescription* pRB = pDataDefs->arrDataDescriptions[i].Data.RigidBodyDescription;
                if (body.compare(pRB->szName) == 0) {
                    RCLCPP_INFO(this->get_logger(), "RigidBody Name : %s", pRB->szName);
                    RCLCPP_INFO(this->get_logger(), "RigidBody ID : %d", pRB->ID);
                    body_id = pRB->ID;
                    return true;
                } else {
                    RCLCPP_DEBUG(this->get_logger(), "RigidBody Name : %s", pRB->szName);
                    RCLCPP_DEBUG(this->get_logger(), "RigidBody ID : %d", pRB->ID);
                }
            }
        }
        RCLCPP_ERROR(this->get_logger(), "No body with name : %s", body.c_str());
        return false;
    }

    void data_handler(sFrameOfMocapData* data) {
        int i=0;
        RCLCPP_DEBUG(this->get_logger(), "FrameID : %d", data->iFrame);
        RCLCPP_DEBUG(this->get_logger(), "Timestamp : %3.2lf", data->fTimestamp);
        // timecode - for systems with an eSync and SMPTE timecode generator - decode to values
        int hour, minute, second, frame, subframe;
        NatNet_DecodeTimecode( data->Timecode, data->TimecodeSubframe, &hour, &minute, &second, &frame, &subframe );
        // decode to friendly string
	    char szTimecode[128] = "";
        NatNet_TimecodeStringify( data->Timecode, data->TimecodeSubframe, szTimecode, 128 );
	    RCLCPP_DEBUG(this->get_logger(), "Timecode : %s", szTimecode);
	    // Rigid Bodies
	    RCLCPP_DEBUG(this->get_logger(), "Rigid Bodies [Count=%d]", data->nRigidBodies);
	    for(i=0; i < data->nRigidBodies; i++)
	    {
            // 0x01 : bool, rigid body was successfully tracked in this frame
            bool bTrackingValid = data->RigidBodies[i].params & 0x01;
		    if (data->RigidBodies[i].ID == this->body_id && bTrackingValid) {
                RCLCPP_DEBUG(this->get_logger(), "Rigid Body [ID=%d  Error=%3.2f  Valid=%d]", 
                data->RigidBodies[i].ID, data->RigidBodies[i].MeanError, bTrackingValid);
        		RCLCPP_DEBUG(this->get_logger(), "\tx\ty\tz\tqx\tqy\tqz\tqw");
		        RCLCPP_DEBUG(this->get_logger(), "\t%3.2f\t%3.2f\t%3.2f\t%3.2f\t%3.2f\t%3.2f\t%3.2f",
                    data->RigidBodies[i].x,
                    data->RigidBodies[i].y,
                    data->RigidBodies[i].z,
                    data->RigidBodies[i].qx,
                    data->RigidBodies[i].qy,
                    data->RigidBodies[i].qz,
                    data->RigidBodies[i].qw);
                // Pose
                auto p = geometry_msgs::msg::PoseStamped();
                p.header.frame_id = world_frame;
                p.header.stamp = this->get_clock()->now();
                p.pose.position.x = data->RigidBodies[i].x;
                p.pose.position.y = data->RigidBodies[i].y;
                p.pose.position.z = data->RigidBodies[i].z;
                p.pose.orientation.x = data->RigidBodies[i].qx;
                p.pose.orientation.y = data->RigidBodies[i].qy;
                p.pose.orientation.z = data->RigidBodies[i].qz;
                p.pose.orientation.w = data->RigidBodies[i].qw;
                pose_pub->publish(p);
                // GeoPoint
                if (this->get_parameter("geoproj").as_bool()) {
                    auto g = geographic_msgs::msg::GeoPoint();
                    cs_tools::Point point = cs_tools::Point(p.pose.position.x, p.pose.position.y, p.pose.position.z);
                    auto geopoint = proj->local_to_spherical(point, local_frame);
                    g.latitude = geopoint.latitude;
                    g.longitude = geopoint.longitude;
                    g.altitude = geopoint.altitude;
                    geopoint_pub->publish(g);
                }
                // TF
                geometry_msgs::msg::TransformStamped tf;
                tf.header.stamp = p.header.stamp;
                tf.header.frame_id = world_frame;
                tf.child_frame_id = robot_frame;
                tf.transform.translation.x = p.pose.position.x;
                tf.transform.translation.y = p.pose.position.y;
                tf.transform.translation.z = p.pose.position.z;
                tf.transform.rotation.x = p.pose.orientation.x;
                tf.transform.rotation.y = p.pose.orientation.y;
                tf.transform.rotation.z = p.pose.orientation.z;
                tf.transform.rotation.w = p.pose.orientation.w;
                tf_br->sendTransform(tf);
            }
	    }
    }

private:
    sNatNetClientConnectParams g_connectParams;
    sServerDescription g_serverDescription;
    int g_analogSamplesPerMocapFrame;
    int body_id;
    rclcpp::Publisher<geometry_msgs::msg::PoseStamped>::SharedPtr pose_pub;
    rclcpp::Publisher<geographic_msgs::msg::GeoPoint>::SharedPtr geopoint_pub;
    std::shared_ptr<tf2_ros::TransformBroadcaster> tf_br;
    rclcpp::SensorDataQoS qos;
    cs_tools::CoordinateType local_frame = cs_tools::CoordinateType::NWU;
    std::shared_ptr<cs_tools::GeoProj> proj;
    std::string world_frame;
    std::string robot_frame;
};
